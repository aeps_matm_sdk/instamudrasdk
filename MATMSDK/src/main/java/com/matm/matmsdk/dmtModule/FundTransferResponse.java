package com.matm.matmsdk.dmtModule;

import java.util.Arrays;
import java.util.Map;

public class FundTransferResponse {

    private String balanceLimitPipe1;

    private String balanceLimitPipe2;

    private String balanceLimitPipe3;

    private String customerId;

    private String status;

    private Map<Integer, PaymentPipe> gateWayList;

    private String name;

    private String mobileNumber;

    private BeneListModel[] beneList;

    private String statusDesc;

    public Map<Integer, PaymentPipe> getGateWayList() {
        return gateWayList;
    }

    public void setGateWayList(Map<Integer, PaymentPipe> gateWayList) {
        this.gateWayList = gateWayList;
    }

    public String getBalanceLimitPipe1 ()
    {
        return balanceLimitPipe1;
    }

    public void setBalanceLimitPipe1 (String balanceLimitPipe1)
    {
        this.balanceLimitPipe1 = balanceLimitPipe1;
    }

    public String getBalanceLimitPipe2 ()
    {
        return balanceLimitPipe2;
    }

    public void setBalanceLimitPipe2 (String balanceLimitPipe2)
    {
        this.balanceLimitPipe2 = balanceLimitPipe2;
    }

    public String getBalanceLimitPipe3 ()
    {
        return balanceLimitPipe3;
    }

    public void setBalanceLimitPipe3 (String balanceLimitPipe3)
    {
        this.balanceLimitPipe3 = balanceLimitPipe3;
    }

    public String getCustomerId ()
    {
        return customerId;
    }

    public void setCustomerId (String customerId)
    {
        this.customerId = customerId;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    /*public GateWayListModel getGateWayList ()
    {
        return gateWayList;
    }

    public void setGateWayList (GateWayListModel gateWayList)
    {
        this.gateWayList = gateWayList;
    }*/

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getMobileNumber ()
    {
        return mobileNumber;
    }

    public void setMobileNumber (String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public BeneListModel[] getBeneList ()
    {
        return beneList;
    }

    public void setBeneList (BeneListModel[] beneList)
    {
        this.beneList = beneList;
    }

    public String getStatusDesc ()
    {
        return statusDesc;
    }



    public void setStatusDesc (String statusDesc)
    {
        this.statusDesc = statusDesc;
    }
    @Override
    public String toString() {
        return "FundTransferResponse{" +
                "balanceLimitPipe1='" + balanceLimitPipe1 + '\'' +
                ", balanceLimitPipe2='" + balanceLimitPipe2 + '\'' +
                ", balanceLimitPipe3='" + balanceLimitPipe3 + '\'' +
                ", customerId='" + customerId + '\'' +
                ", status='" + status + '\'' +
                ", gateWayList=" + gateWayList +
                ", name='" + name + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", beneList=" + Arrays.toString(beneList) +
                ", statusDesc='" + statusDesc + '\'' +
                '}';
    }

}
