package com.matm.matmsdk.dmtModule;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface VerifyOtpApi {
    @POST()
    Call<VerifyOtpResponse> getVerifyOtp(@Header("Authorization") String token, @Body VerifyOtpRequest verifyOtpRequestBody, @Url String url);
}
