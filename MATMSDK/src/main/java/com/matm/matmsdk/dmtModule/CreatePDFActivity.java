package com.matm.matmsdk.dmtModule;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.print.PrintAttributes;
import android.print.PrintManager;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;


import com.matm.matmsdk.Utils.SdkConstants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import isumatm.androidsdk.equitas.R;

public class CreatePDFActivity extends AppCompatActivity {
    private TextView shopNameTxt, dateTxt, contactNoTxt, timeTxt, accNoTxt, beneNameTxt, beneMobNo, bankNametxt, statusTxt, thankyou_txt;
    private LinearLayout transaction_ll, pdfLayout, main_ll;
    private Button pdfBtn, shareBtn;
    CreatePDFActivity activity;
    //Uri uri;
    //Bitmap bitmap;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dmt_pdf_layout);
        isReadStoragePermissionGranted();
        isWriteStoragePermissionGranted();

        shopNameTxt = (TextView) findViewById(R.id.shopNameTxt);
        dateTxt = (TextView) findViewById(R.id.dateTxt);
        contactNoTxt = findViewById(R.id.contactNoTxt);
        timeTxt = findViewById(R.id.timeTxt);
        accNoTxt = findViewById(R.id.accNoTxt);
        beneNameTxt = findViewById(R.id.beneNameTxt);
        beneMobNo = findViewById(R.id.beneMobNo);
        bankNametxt = findViewById(R.id.bankNametxt);
        statusTxt = findViewById(R.id.statusTxt);
        main_ll = findViewById(R.id.main_ll);
        thankyou_txt = findViewById(R.id.thankyou_txt);

        transaction_ll = (LinearLayout) findViewById(R.id.transaction_ll);
        pdfBtn = findViewById(R.id.pdfBtn);
        shareBtn = findViewById(R.id.shareBtn);


        ArrayList<TransactionDetails> filelist = (ArrayList<TransactionDetails>) getIntent().getSerializableExtra("data");

        String benename = getIntent().getStringExtra("bene_name");
        String benePhone = getIntent().getStringExtra("bene_mob_no");
        String acc_no = getIntent().getStringExtra("acc_no");
        String bene_bank = getIntent().getStringExtra("bank_name");

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);

        SimpleDateFormat df2 = new SimpleDateFormat("HH:mm:ss");
        String formattedDate2 = df2.format(c);

        shopNameTxt.setText(SdkConstants.SHOP_NAME);
        dateTxt.setText(formattedDate);
        contactNoTxt.setText(SdkConstants.USER_MOBILE_NO);
        timeTxt.setText(formattedDate2);

        accNoTxt.setText(acc_no);
        beneNameTxt.setText(benename);
        beneMobNo.setText(benePhone);
        bankNametxt.setText(bene_bank);
        statusTxt.setText("Status Successful.");
        thankyou_txt.setText("Thank you\n " + SdkConstants.BRAND_NAME);

        //-----------------Dynamic Order View-------------
        for (int i = 0; i < filelist.size(); i++) {

            populateOrder(filelist.get(i).getTransaction_amount(), filelist.get(i).getTracking_no(), filelist.get(i).getTransaction_type());
        }


        //bitmap = createBitmapFromView(main_ll);

        pdfBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrintManager printManager = (PrintManager) CreatePDFActivity.this.getSystemService(PRINT_SERVICE);
                PrintAttributes newAttributes = new PrintAttributes.Builder().
                        setMediaSize(PrintAttributes.MediaSize.ISO_A4).
                        setMinMargins(new PrintAttributes.Margins(1000, 200, 0, 20)).
                        build();
                printManager.print("order_recept",
                        new PdfFragmentPrintDocumentAdapter(CreatePDFActivity.this, main_ll), newAttributes);
            }
        });
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                takeScreen();

            }
        });
    }

    //-----------------Dynamic Order View-------------
    private void populateOrder(String transtion_amount, String tracking_no, String transaction_type) {
        LayoutInflater layoutInflater = getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.dynamic_orderview, null, false);
        TextView transAmount = (TextView) view.findViewById(R.id.transAmount);
        TextView tracking_noo = (TextView) view.findViewById(R.id.tracking_no);
        TextView transType = (TextView) view.findViewById(R.id.transType);
        transAmount.setText(transtion_amount);
        tracking_noo.setText(tracking_no);
        transType.setText(transaction_type);
        transaction_ll.addView(view);
    }


    //-------------------------------------
    public boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    public boolean isWriteStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }


    //-----------------------------------------------------
    public void takeScreen() {
        Bitmap bitmap = loadBitmapFromView(this, main_ll); //get Bitmap from the view
        String mPath = Environment.getExternalStorageDirectory() + File.separator + "screen_" + System.currentTimeMillis() + ".jpeg";
        File imageFile = new File(mPath);
        OutputStream fout = null;
        try {
            fout = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fout);

            Uri uri = getImageUri(CreatePDFActivity.this, bitmap);

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setType("image/png");
            startActivity(intent);


            fout.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } /*finally {
            fout.close();
        }*/
    }

    //--
    public static Bitmap loadBitmapFromView(Context context, View v) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        v.measure(View.MeasureSpec.makeMeasureSpec(dm.widthPixels, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(dm.heightPixels, View.MeasureSpec.EXACTLY));
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        Bitmap returnedBitmap = Bitmap.createBitmap(v.getMeasuredWidth(),
                v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(returnedBitmap);
        v.draw(c);

        return returnedBitmap;
    }


//==


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    public Bitmap createBitmapFromView(View v) {
        v.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        v.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(v.getMeasuredWidth(),
                v.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bitmap);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return bitmap;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

}
