package com.matm.matmsdk.dmtModule;

public class AddNPayResponse {

    private String status;

    private String statusDesc;

    private String beneName;

    private String txnId;

    public String getstatus ()
    {
        return status;
    }

    public void setstatus (String status)
    {
        this.status = status;
    }

    public String getstatusDesc ()
    {
        return statusDesc;
    }

    public void setstatusDesc (String statusDesc)
    {
        this.statusDesc = statusDesc;
    }

    public String getBeneName ()
    {
        return beneName;
    }

    public void setBeneName (String beneName)
    {
        this.beneName = beneName;
    }

    public String gettxnId ()
    {
        return txnId;
    }

    public void settxnId (String txnId)
    {
        this.txnId = txnId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", statusDesc = "+statusDesc+", beneName = "+beneName+", txnId = "+txnId+"]";
    }

}
