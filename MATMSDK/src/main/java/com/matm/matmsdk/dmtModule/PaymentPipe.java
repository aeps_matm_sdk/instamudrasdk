package com.matm.matmsdk.dmtModule;

public class PaymentPipe {

    private long id;
    private String pipeType;
    private boolean pipeFlag;
    private boolean splitService;
    private boolean verificationFlag;
    private double balance;
    private PaymentChannel channelList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPipeType() {
        return pipeType;
    }

    public void setPipeType(String pipeType) {
        this.pipeType = pipeType;
    }

    public boolean isPipeFlag() {
        return pipeFlag;
    }

    public void setPipeFlag(boolean pipeFlag) {
        this.pipeFlag = pipeFlag;
    }

    public boolean isSplitService() {
        return splitService;
    }

    public void setSplitService(boolean splitService) {
        this.splitService = splitService;
    }

    public boolean isVerificationFlag() {
        return verificationFlag;
    }

    public void setVerificationFlag(boolean verificationFlag) {
        this.verificationFlag = verificationFlag;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public PaymentChannel getChannelList() {
        return channelList;
    }

    public void setChannelList(PaymentChannel channelList) {
        this.channelList = channelList;
    }

    @Override
    public String toString() {
        return "PaymentPipe{" +
                "id=" + id +
                ", pipeType='" + pipeType + '\'' +
                ", pipeFlag=" + pipeFlag +
                ", splitService=" + splitService +
                ", verificationFlag=" + verificationFlag +
                ", balance=" + balance +
                ", channelList=" + channelList +
                '}';
    }




}
