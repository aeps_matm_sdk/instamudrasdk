package com.matm.matmsdk.dmtModule;

public class VerifyOtpRequest {
    private String otp;

    public VerifyOtpRequest(String otp, String mobileNumber) {
        this.otp = otp;
        this.mobileNumber = mobileNumber;
    }

    private String mobileNumber;

    public String getOtp ()
    {
        return otp;
    }

    public void setOtp (String otp)
    {
        this.otp = otp;
    }

    public String getMobileNumber ()
    {
        return mobileNumber;
    }

    public void setMobileNumber (String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [otp = "+otp+", mobileNumber = "+mobileNumber+"]";
    }
}
