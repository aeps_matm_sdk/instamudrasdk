package com.matm.matmsdk.dmtModule.bankList;


import android.content.Context;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.matm.matmsdk.Utils.SdkConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DMTBankPresenter implements DMTBankContract.UserActionsListener {
    private static final String TAG = DMTBankPresenter.class.getSimpleName();
    private DMTBankContract.View banknameView;

    public DMTBankPresenter(DMTBankContract.View banknameView) {
        this.banknameView = banknameView;
    }

    @Override
    public void loadBankNamesList(Context context) {
        banknameView.showLoader();
        ArrayList<DMTBankModel> bankNamesArrayList = new ArrayList<>();

        JSONObject obj = new JSONObject();
        try {
            obj.put("getAll", true);
            AndroidNetworking.post("https://us-central1-iserveustaging.cloudfunctions.net/bankNamebusiness/api/v1/bankNameGetbank")
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject object = new JSONObject(response.toString());
                                JSONObject data = object.getJSONObject("data");
                                SdkConstants.BANK_NAME = data.toString();
                                JSONArray bankArray = data.getJSONArray("banks");
                                Log.e(TAG, "onResponse: size "+bankArray.length() );
                                for (int i = 0; i < bankArray.length(); i++) {
                                    DMTBankModel model = new DMTBankModel();
                                    JSONObject bank = bankArray.getJSONObject(i);
                                    String bankName = bank.getString("bankName");
                                    String IMPS_AVAILABILITY = bank.getString("IMPS_AVAILABILITY");
                                    String BANKCODE = bank.getString("BANKCODE");
                                    String ACCOUNT_VERAFICATION_AVAILABLE = bank.getString("ACCOUNT_VERAFICATION_AVAILABLE");
                                    String FLAG = bank.getString("FLAG");
                                    String PATTERN = bank.getString("PATTERN");
                                    boolean active = bank.getBoolean("active");
                                    String NEFT_AVAILABILITY = bank.getString("NEFT_AVAILABILITY");

                                    model.setBankName(bankName);
                                    model.setIMPS_AVAILABILITY(IMPS_AVAILABILITY);
                                    model.setBANKCODE(BANKCODE);
                                    model.setACCOUNT_VERAFICATION_AVAILABLE(ACCOUNT_VERAFICATION_AVAILABLE);
                                    model.setFLAG(FLAG);
                                    model.setPATTERN(PATTERN);
                                    model.setActive(active);
                                    model.setNEFT_AVAILABILITY(NEFT_AVAILABILITY);

                                    bankNamesArrayList.add(model);
                                }
                                banknameView.hideLoader();
                                banknameView.bankNameListReady(bankNamesArrayList);
                                banknameView.showBankNames();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e(TAG, "onResponse: json exp "+e.toString() );
                                banknameView.hideLoader();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e(TAG, "onError: an error "+anError.getErrorBody() );
                            banknameView.hideLoader();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
