package com.matm.matmsdk.dmtModule;

import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import isumatm.androidsdk.equitas.R;

public class BeneRecycleAdapter extends RecyclerView.Adapter<BeneRecycleAdapter.ReportViewhOlder> implements Filterable {

    private List<BeneModel> reportModelListFiltered;
    private List<BeneModel> reportModels;
    TransferFragment frag;


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    reportModelListFiltered = reportModels;
                } else {
                    List<BeneModel> filteredList = new ArrayList<>();
                    for (BeneModel row : reportModels) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match

                        if (row.getBeneMobileNo().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getCustomerNumber().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    reportModelListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = reportModelListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                reportModelListFiltered = (ArrayList<BeneModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ReportViewhOlder extends RecyclerView.ViewHolder {
        public TextView name, accountNo, bankName;
        ImageView view_delete;

        public ReportViewhOlder(View view) {
            super(view);
            name = view.findViewById(R.id.ben_name);
            accountNo = view.findViewById(R.id.ben_account);
            bankName = view.findViewById(R.id.ben_bank_name);
            view_delete = view.findViewById(R.id.ben_delete);
        }
    }

    public BeneRecycleAdapter(TransferFragment frag, List<BeneModel> reportModels) {
        this.reportModels = reportModels;
        this.reportModelListFiltered = reportModels;
        this.frag = frag;

    }

    public ReportViewhOlder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bene_items, parent, false);
        return new ReportViewhOlder(itemView);
    }

    public void onBindViewHolder(ReportViewhOlder holder, int position) {
        final BeneModel beneModel = reportModelListFiltered.get(position);
        holder.name.setText(beneModel.getBeneName());
        holder.accountNo.setText("Account No. : " + beneModel.getAccountNo().replaceAll("-", ""));
        holder.bankName.setText(beneModel.getBankName() + " (" + beneModel.getIfscCode() + ")");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag.itemOncliclickView(beneModel);
            }
        });

        holder.view_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(frag);
                builder.setMessage("Do you want to delete beneficiary detail?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                frag.itemOncliclickDelete(beneModel, position);
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.setTitle("Alert!");
                alert.show();
            }
        });
    }

    public int getItemCount() {
        return reportModelListFiltered.size();
    }
}


