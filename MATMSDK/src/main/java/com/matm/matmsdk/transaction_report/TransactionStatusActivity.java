package com.matm.matmsdk.transaction_report;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.matm.matmsdk.ChooseCard.ChooseCardActivity;

import isumatm.androidsdk.equitas.R;

import com.matm.matmsdk.FileUtils;
import com.matm.matmsdk.Service.BankResponse;
import com.matm.matmsdk.Utils.PAXScreen;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.transactionstatus.TransactionStatusNewActivity;
import com.matm.matmsdk.dmtModule.bluetooth.BluetoothPrinter;
import com.matm.matmsdk.permission.PermissionsActivity;
import com.matm.matmsdk.permission.PermissionsChecker;
import com.paxsz.easylink.api.EasyLinkSdkManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.matm.matmsdk.permission.PermissionsActivity.PERMISSION_REQUEST_CODE;
import static com.matm.matmsdk.permission.PermissionsChecker.REQUIRED_PERMISSION;

public class TransactionStatusActivity extends AppCompatActivity {
    private LinearLayout ll_maiin;
    private TextView statusMsgTxt,statusDescTxt;
    private ImageView status_icon;
    private TextView appl_name,a_id,date_time,rref_num,mid,tid,txnid,invoice,card_id,appr_code,card_no,card_amount,card_transaction_amount,txn_id;
    public EasyLinkSdkManager manager;
    private Button backBtn,downloadBtn,printBtn;
    ChooseCardActivity chooseCardActivity;
    LinearLayout ll13,ll12;
    PermissionsChecker checker;
    Context mContext;
    BluetoothAdapter B;
    private int STORAGE_PERMISSION_CODE=1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_status);
        ll_maiin = findViewById(R.id.ll_maiin);
        manager = EasyLinkSdkManager.getInstance(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        //Runtime permission request required if Android permission >= Marshmallow
        checker = new PermissionsChecker(this);
        mContext = getApplicationContext();

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }*/

        ll13 = findViewById(R.id.ll13);
        ll12 = findViewById(R.id.ll12);
        B=BluetoothAdapter.getDefaultAdapter();
        card_transaction_amount = findViewById(R.id.card_transaction_amount);
        statusMsgTxt = findViewById(R.id.statusMsgTxt);
        status_icon = findViewById(R.id.status_icon);
        appl_name = findViewById(R.id.appl_name);
        a_id = findViewById(R.id.a_id);
        rref_num = findViewById(R.id.rref_num);
        mid = findViewById(R.id.mid);
        tid = findViewById(R.id.tid);
        txnid = findViewById(R.id.txnid);
        invoice = findViewById(R.id.invoice);
        card_id = findViewById(R.id.card_id);
        appr_code = findViewById(R.id.appr_code);
        card_no = findViewById(R.id.card_no);
        card_amount = findViewById(R.id.card_amount);
        statusDescTxt = findViewById(R.id.statusDescTxt);
        backBtn = findViewById(R.id.backBtn);
        downloadBtn = findViewById(R.id.downloadBtn);
        printBtn = findViewById(R.id.printBtn);
        date_time = findViewById(R.id.date_time);
        txn_id = findViewById(R.id.txn_id);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd : HH.mm.ss");
        String currentDateandTime = sdf.format(new Date());
        date_time.setText(currentDateandTime);
        String transaction_type = getIntent().getStringExtra("TRANSACTION_TYPE");
        String transaction_amount = getIntent().getStringExtra("TRANSACTION_AMOUNT");
        String TRANSACTION_ID = getIntent().getStringExtra("TRANSACTION_ID");
        String applName = getIntent().getStringExtra("APP_NAME");
        String aId = getIntent().getStringExtra("AID");
        String prefNum = getIntent().getStringExtra("RRN_NO");
        String MID = getIntent().getStringExtra("MID");
        String TID = getIntent().getStringExtra("TID");
        String TXN_ID = getIntent().getStringExtra("TXN_ID");
        String INVOICE = getIntent().getStringExtra("INVOICE");
        String CARD_TYPE = getIntent().getStringExtra("CARD_TYPE");
        String APPR_CODE = getIntent().getStringExtra("APPR_CODE");
        String CARD_NUMBER = getIntent().getStringExtra("CARD_NUMBER");
        String AMOUNT = getIntent().getStringExtra("AMOUNT");
        String RESPONSE_CODE = getIntent().getStringExtra("RESPONSE_CODE");
        RESPONSE_CODE = RESPONSE_CODE.substring(2);

        if(AMOUNT.equalsIgnoreCase("0")){
            AMOUNT = "0.00";
        }else{
            AMOUNT = replaceWithZero(AMOUNT);
        }

        System.out.println(">>>----"+AMOUNT);

        String[] splitAmount = CARD_NUMBER.split("D");
        CARD_NUMBER = splitAmount[0];

        String firstnum = CARD_NUMBER.substring(0,2);
        String middlenum = CARD_NUMBER.substring(2,CARD_NUMBER.length()-2);
        String lastNum = CARD_NUMBER.replace(firstnum+middlenum,"");

        System.out.println(">>>---"+firstnum);
        System.out.println(">>>---"+middlenum);
        System.out.println(">>>---"+lastNum);

        if(transaction_type.equalsIgnoreCase("cash")){
            ll13.setVisibility(View.VISIBLE);
            card_amount.setText(AMOUNT);
            card_transaction_amount.setText(replaceWithZero(transaction_amount));
        }else{
            ll13.setVisibility(View.GONE);
            ll12.setVisibility(View.VISIBLE);
            card_amount.setText(AMOUNT);
        }


       // CARD_NUMBER =
        String flag = getIntent().getStringExtra("flag");
        if(flag.equalsIgnoreCase("failure")){
            ll_maiin.setBackgroundColor(Color.parseColor("#D94237"));
            status_icon.setImageResource(R.drawable.ic_errorrr);
            statusMsgTxt.setText("Failed.");
            //statusDescTxt.setText("UnFortunatly Paymet was rejected.");
            backBtn.setBackgroundResource(R.drawable.button_background_fail);
            downloadBtn.setBackgroundResource(R.drawable.button_background_fail);
            printBtn.setBackgroundResource(R.drawable.button_background_fail);
            BankResponse.showStatusMessage(manager,RESPONSE_CODE,statusDescTxt);
            PAXScreen.showFailure(manager);
            appl_name.setText(applName);
            a_id.setText(aId);
            rref_num.setText(prefNum);
            mid.setText(MID);
            txn_id.setText(TRANSACTION_ID);
            tid.setText(TID);
            txnid.setText(TXN_ID);
            invoice.setText(INVOICE);
            card_id.setText(CARD_TYPE);
            appr_code.setText(APPR_CODE);
            card_no.setText(CARD_NUMBER);
            card_amount.setText("N/A");

            String transactionType = "";
            if(SdkConstants.transactionType.equalsIgnoreCase("0")){
                transactionType = "BalanceEnquiry Failled!! ";
            }else{
                transactionType = "CashWithdraw Failled!! ";
            }

            String str ="";
            str = statusDescTxt.getText().toString();

            String responseData = generateJsonData(transactionType,str,prefNum,CARD_NUMBER,AMOUNT,TID);
            SdkConstants.responseData =responseData;
        }


       else {
            //Show Success
            PAXScreen.showSuccess(manager);
            appl_name.setText(applName);
            a_id.setText(aId);
            txn_id.setText(TRANSACTION_ID);
            rref_num.setText(prefNum);
            mid.setText(MID);
            tid.setText(TID);
            txnid.setText(TXN_ID);
            invoice.setText(INVOICE);
            card_id.setText(CARD_TYPE);
            appr_code.setText(APPR_CODE);
            card_no.setText(CARD_NUMBER);
            card_amount.setText(AMOUNT);

            String transactionType = "";
            if (SdkConstants.transactionType.equalsIgnoreCase("0")) {
                transactionType = "BalanceEnquiry Successful!! ";
            } else {
                transactionType = "CashWithdraw Successful!! ";
            }

            String str ="";
            str = statusDescTxt.getText().toString();

            String responseData = generateJsonData(transactionType,str, prefNum, CARD_NUMBER, AMOUNT, TID);
            SdkConstants.responseData = responseData;


        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /*downloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checker.lacksPermissions(REQUIRED_PERMISSION)) {
                    PermissionsActivity.startActivityForResult(TransactionStatusActivity.this, PERMISSION_REQUEST_CODE, REQUIRED_PERMISSION);
                } else {
                    Date date = new Date();
                    long timeMilli = date.getTime();
                    System.out.println("Time in milliseconds using Date class: " + String.valueOf(timeMilli));
                    showPdf();

                }

                if(ContextCompat.checkSelfPermission(TransactionStatusActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)

                {
                    Toast.makeText(TransactionStatusActivity.this,"Already Permission is there",Toast.LENGTH_SHORT).show();

                }else if (ContextCompat.checkSelfPermission(TransactionStatusActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                    isStoragePermissionGranted();
                    showPdf();
                }else {
                    requestPermission();
                }
                }

        });*/

        downloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checker.lacksPermissions(REQUIRED_PERMISSION)) {
                    PermissionsActivity.startActivityForResult(TransactionStatusActivity.this, PERMISSION_REQUEST_CODE, REQUIRED_PERMISSION);
                } else {
                    Date date = new Date();
                    long timeMilli = date.getTime();
                    System.out.println("Time in milliseconds using Date class: " + String.valueOf(timeMilli));
                    createPdf(FileUtils.getAppPath(mContext) + String.valueOf(timeMilli)+"Order_Receipt.pdf");
                }
            }
        });
        printBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    BluetoothDevice bluetoothDevice = SdkConstants.bluetoothDevice;
                    if (bluetoothDevice != null) {

                        if (!B.isEnabled()) {
                           /* Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(turnOn, 0);*/
                            finish();
                            Toast.makeText(getApplicationContext(), "Your Bluetooth is OFF .",Toast.LENGTH_LONG).show();
                        } else {
//                            Toast.makeText(getApplicationContext(), "Bluetooth is already on", Toast.LENGTH_LONG).show();
                            callBluetoothFunction(txn_id.getText().toString(), date_time.getText().toString(), rref_num.getText().toString(),mid.getText().toString(),tid.getText().toString(),card_id.getText().toString(), card_no.getText().toString(), card_transaction_amount.getText().toString(), bluetoothDevice);
                        }

                    } else {
                        finish();
                        //Toast.makeText(TransactionStatusActivity.this, "Please connect the printer", Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "Please connect the printer",Toast.LENGTH_LONG).show();
                    }
                /* else {
                    showBrandSetAlert();
                }*/

            }
        });
    }
    public String replaceWithZero(String s) {
        //int length = s.length();
        //Check whether or not the string contains at least four characters; if not, this method is useless
        float amount = Integer.valueOf(s)/100F;
        DecimalFormat formatter = new DecimalFormat("##,##,##,##0.00");
        return formatter.format(Double.parseDouble(String.valueOf(amount)));
        // return String.valueOf(amount);
    }
    public String generateJsonData(String status,String statusDesc, String rrn,String cardno,String bal,String terminalId){
        String jdata = "";
        JSONObject obj = new JSONObject();
        try {
            obj.put("TransactionStatus",status);
            obj.put("StatusDescription",statusDesc);
            obj.put("RRN",rrn);
            obj.put("CardNumber",cardno);
            obj.put("Balance",bal);
            obj.put("TerminalID",terminalId);
            jdata = obj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jdata;
    }
    public void createPdf(String dest) {
        if (new File(dest).exists()) {
            new File(dest).delete();
        }
        try {
            /**
             * Creating Document
             */
            Document document = new Document();

            // Location to save
           PdfWriter writer= PdfWriter.getInstance(document, new FileOutputStream(dest));
            // Open to write
            document.open();

            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("");
            document.addCreator("");

            /**
             * How to USE FONT....
             */
            BaseFont urName = BaseFont.createFont("assets/fonts/brandon_medium.otf", "UTF-8", BaseFont.EMBEDDED);

            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));

            BaseFont bf = BaseFont.createFont(
                    BaseFont.TIMES_ROMAN,
                    BaseFont.CP1252,
                    BaseFont.EMBEDDED);
            Font font = new Font(bf, 30);
            Font font2 = new Font(bf, 26);

            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            PdfPCell cell = new PdfPCell(new Paragraph("Transaction Receipt",font));
            cell.setColspan(2); // colspan
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            table.addCell(new Paragraph("Transaction ID",font)); // how to change cell to have different font and bold and background color
            table.addCell(new Paragraph(txn_id.getText().toString(),font2)); // how to change cell to have different font and bold and background color
            table.addCell(new Paragraph("Date/Time",font));
            table.addCell(new Paragraph(date_time.getText().toString(),font2));
            table.addCell(new Paragraph("RREF NUM",font));
            table.addCell(new Paragraph(rref_num.getText().toString(),font2));
            table.addCell(new Paragraph("MID",font));
            table.addCell(new Paragraph(mid.getText().toString(),font2));
            table.addCell(new Paragraph("Terminal ID",font));
            table.addCell(new Paragraph(tid.getText().toString(),font2));
            table.addCell(new Paragraph("Card Type",font));
            table.addCell(new Paragraph(card_id.getText().toString(),font2));
            table.addCell(new Paragraph("Card Num",font));
            table.addCell(new Paragraph(card_no.getText().toString(),font2));
            table.addCell(new Paragraph("Balance Amount",font));
            table.addCell(new Paragraph(card_amount.getText().toString(),font2));
            table.addCell(new Paragraph("Transacted Amount",font));
            table.addCell(new Paragraph(card_transaction_amount.getText().toString(),font2));
            document.add(table);
            // Title Order Details...
            // Adding Title....
            Font mOrderDetailsTitleFont;
            if(statusMsgTxt.getText().toString().equalsIgnoreCase("Failed.")){
                mOrderDetailsTitleFont = new Font(urName, 40.0f, Font.NORMAL, BaseColor.RED);

            }else{
                mOrderDetailsTitleFont = new Font(urName, 40.0f, Font.NORMAL, BaseColor.GREEN);
            }

            Chunk mOrderDetailsTitleChunk = new Chunk(statusMsgTxt.getText().toString(), mOrderDetailsTitleFont);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderDetailsTitleParagraph);
            document.close();
            writer.close();
            //Toast.makeText(mContext,"PDF saved in the internal storage",FileUtils.getAppPath(mContext)+"Order_Receipt.pdf"Toast.LENGTH_SHORT).show();
            //File file = new File("/storage/emulated/0/PDF/Order_Receipt.pdf");
            //Toast.makeText(mContext,"pdf saved in the inter storage"+FileUtils.getAppPath(mContext)+"Order_Receipt.pdf",Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext, "PDF saved in the internal storage", Toast.LENGTH_SHORT).show();
            showPdf(dest);
        }
        catch (IOException | DocumentException ie) {
            Log.e("createPdf: Error ","" + ie.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(mContext, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == PermissionsActivity.PERMISSIONS_GRANTED) {
            Toast.makeText(mContext, "Permission Granted to Save", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "Permission not granted, Try again!", Toast.LENGTH_SHORT).show();
        }
    }


    private void showBrandSetAlert(){
        try{
            AlertDialog.Builder builder1 = new AlertDialog.Builder(TransactionStatusActivity.this);
            builder1.setMessage("Unable to download/print the receipt. Please contact admin.");
            builder1.setTitle("Warning!!!");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "GOT IT",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }catch (Exception e){

        }
    }

    private void callBluetoothFunction(final String txnId, final String date, final String reffNo,final String mid,final String terminalId, final String type,final String cardNumber, final String transactionAmt,  BluetoothDevice bluetoothDevice) {


        final BluetoothPrinter mPrinter = new BluetoothPrinter(bluetoothDevice);
        mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {

            @Override
            public void onConnected() {
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.printText("-----Transaction Report-----");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(statusMsgTxt.getText().toString());
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.printText("TXNId: " + txn_id.getText().toString());
                mPrinter.addNewLine();
                mPrinter.printText("Date/Time: " + date);
                mPrinter.addNewLine();
                mPrinter.printText("Ref No.: " + reffNo);
                mPrinter.addNewLine();
                mPrinter.printText("Mid : " + mid);
                mPrinter.addNewLine();
                mPrinter.printText("TerminalID: " + terminalId);
                mPrinter.addNewLine();
                mPrinter.printText("CardNumber : " + cardNumber);
                mPrinter.addNewLine();
                mPrinter.printText("CardAmount : " + card_amount.getText().toString());
                mPrinter.addNewLine();
                mPrinter.printText("TransactionAmount : " + transactionAmt);
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setBold(true);
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("Thank You");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("-----------------------------------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.finish();
            }

            @Override
            public void onFailed() {
                Log.d("BluetoothPrinter", "Connection failed");
                // finish();
                Toast.makeText(TransactionStatusActivity.this, "Please switch on bluetooth printer", Toast.LENGTH_SHORT).show();
            }
        });


    }
    public void showPdf(String path) {
        //String path = "/storage/emulated/0/Download/" + "Order_Receipt.pdf";
        File file= new File(path);
        if (file.exists()) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(file);
            intent.setDataAndType(uri, "application/pdf");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(TransactionStatusActivity.this, "No Application available to view pdf", Toast.LENGTH_LONG).show();
            }
        }
        isStoragePermissionGranted();
        requestPermission();

    }
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission is granted");
                return true;
            } else {

                System.out.println("Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                return false;
            }
        }
        else {
            //permission is automatically granted on sdk upon installation
            System.out.println("Permission is granted");
            return true;
        }
    }
    public void requestPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE)){
            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setTitle("")
                    .setMessage("")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(getApplicationContext(),
                                    "You clicked on OK", Toast.LENGTH_SHORT).show();
                        }
                    }).show();
        }else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},STORAGE_PERMISSION_CODE);
        }


    }
}




